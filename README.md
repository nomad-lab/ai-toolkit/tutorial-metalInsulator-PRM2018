# analytics-metalInsulator-PRM2018

This tutorial shows how to find descriptive parameters (short formulas) for the classification of materials properties. As an example, we address the classification of elemental and binary systems Ax​​By​​ into metals and non metals